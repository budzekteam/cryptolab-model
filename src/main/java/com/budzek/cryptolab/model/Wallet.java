package com.budzek.cryptolab.model;

import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor //temprorary, refactor usage to builder
@NoArgsConstructor
public class Wallet {
    String id;
    String name;
    WalletItem[] items;

    public JsonObject toJson(){
        return JsonObject.mapFrom(this);
    }
}


