package com.budzek.cryptolab.model;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class WalletsDiff {
    Set<Wallet> removed;
    Set<Wallet> added;
    Set<Wallet> modified;
}
