package com.budzek.cryptolab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)

@DataObject

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CryptolabUser extends MongoObject{
    @JsonProperty("user_id")
    ObjectId userId;
    String login;
    List<Wallet> wallets;
    String token;

    public CryptolabUser(JsonObject json) {
        System.out.println(json);
        this.id = id;
        this.login = login;
        this.wallets = wallets;
    }

    public JsonObject toJson() {
        return new JsonObject();
    }
}


