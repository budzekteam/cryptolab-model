package com.budzek.cryptolab.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ObjectId {

    public ObjectId() {
    }

    public ObjectId(String oid) {
        this.oid = oid;
    }

    @JsonProperty("$oid")
    String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }
}
