package com.budzek.cryptolab.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor //temprorary, refactor usage to builder
@NoArgsConstructor
public class WalletItem {
    String currency;
    Double amount;
}
