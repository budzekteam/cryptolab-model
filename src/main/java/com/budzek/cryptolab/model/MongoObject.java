package com.budzek.cryptolab.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MongoObject {

    public MongoObject() {
    }

    public MongoObject(ObjectId id) {
        this.id = id;
    }

    @JsonProperty("_id")
    ObjectId id;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
