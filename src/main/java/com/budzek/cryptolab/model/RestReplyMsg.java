package com.budzek.cryptolab.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@DataObject
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestReplyMsg {
    public enum Type {SUCCESS, FAILURE}

    Type type;
    String msg;
    JsonObject value;

    //The constructor is needed because there is a problem mapping via JsonObject.mapFrom() when there is an embedded JsonObject property
    public RestReplyMsg(JsonObject json){
        this.setValue(json.getJsonObject("value"));
        this.setMsg(json.getString("msg"));
        this.setType(Type.valueOf(json.getString("type")));
    }

    public JsonObject toJson(){
        return JsonObject.mapFrom(this);
    }

    public static JsonObject success(String msg){
        return success(msg, null);
    }

    public static JsonObject success(String msg, JsonObject value){
        return RestReplyMsg.builder().type(Type.SUCCESS).msg(msg).value(value).build().toJson();
    }

    public static JsonObject failure(String msg){
        return RestReplyMsg.builder().type(Type.FAILURE).msg(msg).build().toJson();
    }
}
