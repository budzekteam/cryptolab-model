package com.budzek.cryptolab.model;

import com.fasterxml.jackson.annotation.JsonRawValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Builder;
import io.vertx.core.json.JsonObject;
import lombok.NoArgsConstructor;

import java.util.Arrays;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WalletMsg {
    public enum Type {
        NEW, UPDATE, REMOVE, VALUE;

        public boolean oneOf(Type... types) {
            return Arrays.asList(types).contains(this);
        }
    }

    //is userId needed ? probably not, maybe token would be useful ?
    String userId;
    String walletId;
    String exchangeProvider;
    Type type;
    JsonObject value;

    //The constructor is needed because there is a problem mapping via JsonObject.mapFrom() when there is an embedded JsonObject property
    public WalletMsg(JsonObject json) {
        this.setValue(json.getJsonObject("value"));
        this.setUserId(json.getString("userId"));
        this.setWalletId(json.getString("walletId"));
        this.setExchangeProvider(json.getString("exchangeProvider"));
        this.setType(Type.valueOf(json.getString("type")));
    }

    public JsonObject toJson() {
        return new JsonObject()
                .put("userId", userId)
                .put("walletId", walletId)
                .put("exchangeProvider", exchangeProvider)
                .put("type", type)
                .put("value", value);
    }

    public <T> T valueAs(Class<T> clazz){
        return this.value.mapTo(clazz);
    }
}
